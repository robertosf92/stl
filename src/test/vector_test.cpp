/*
 * vector_test.cpp
 *
 * Copyright 2023 Roberto Sánchez <robertosanchez_9@protonmail.com>
 *
 * This file is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or  any later version.
 *
 * This file is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: LGPL-3.0-or-later
 */

#include <iostream>
#include <cassert>
#include <cstring>
#include "../stlvector.h"

void test_memory_operations()
{

    STL::vector<int> v;
    STL::vector<int> v2(10);
    STL::vector<int> v3(10,5);

    assert(v.empty());
    assert(v2.empty());
    assert(!v3.empty());

    assert(v3.size() == 10);
    assert(v2.capacity()==10);
    assert(v3.capacity()==10);

}
void test_access_operations()
{
    STL::vector<double> v;
    try{
        v.at(1);
    }
    catch(std::out_of_range e)
    {
        assert(strcmp(e.what(),"Could not access element! Position outside bounds!") == 0);
    }
    v.push_back(1.0f);
    assert(v.at(0) == v[0]);
    assert(v.at(0) == v.front());
    v.push_back(32.5f);
    assert(v.at(1) == v.back());
    assert(v.data() != nullptr);
    assert(v.data()[0] == v.front());
}

void test_capacity_operations()
{
    STL::vector<int> v;
    assert(v.empty());

    v.push_back(1);
    v.push_back(2);
    v.push_back(3);

    assert(v.size() == 3);
    assert(v.capacity() == 4);

    v.reserve(10);
    assert(v.capacity()==10);

    v.reserve(5);
    assert(v.capacity()==10);

    v.shrink_to_fit();
    assert(v.capacity() == v.size());

}

void test_modifiers()
{

}

int main(int argc, char ** argv)
{

    test_memory_operations();
    test_access_operations();
    test_capacity_operations();

}
