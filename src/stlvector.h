/* vector.h
*
* Copyright 2023 Roberto Sánchez <robertosanchez_9@protonmail.com>
*
* This file is free software; you can redistribute it and/or modify it
* under the terms of the GNU Lesser General Public License as
* published by the Free Software Foundation; either version 3 of the
* License, or any later version.
*
* This file is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
* Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public
* License along with this program.  If not, see <http://www.gnu.org/licenses/>.
*
* SPDX-License-Identifier: LGPL-3.0-or-later
*/

#include <cstddef>
#include <stdexcept>
#include <compare>
#include <initializer_list>
#include <iterator>

#ifndef STLVECTOR_H_
#define STLVECTOR_H_



namespace STL{

template <class T, class Allocator=std::allocator<T>> class vector {
  private:
  T* m_ptr;
  Allocator m_alloc;
  size_t m_size;
  size_t m_last;
  public:

  //Memory-related operations
  vector< T, Allocator>() : m_size(0), m_last(0) {};
  vector< T, Allocator>(size_t size) : m_size(size), m_last(0)
  {
      m_ptr = std::allocator_traits<decltype(m_alloc)>::allocate(m_alloc, m_size);
  };
  vector< T, Allocator>(size_t size, T value) : m_size(size), m_last(0)
  {
      m_ptr = std::allocator_traits<decltype(m_alloc)>::allocate(m_alloc, m_size);
      for(; m_last<m_size; m_last++)
          std::allocator_traits<decltype(m_alloc)>::construct(m_alloc, &m_ptr[m_last],value);
  };
  ~vector< T, Allocator>(void)
  {
    for(auto i=0; i<m_last; i++){
      std::allocator_traits<decltype(m_alloc)>::destroy(m_alloc, &m_ptr[i]);
    }
     if(m_size != 0)
      std::allocator_traits<decltype(m_alloc)>::deallocate(m_alloc, m_ptr, m_size);
  };

  vector& operator=(const vector& other)
  {
    auto difference = other.m_size - m_size;

    if(difference < 0)
    {
      for(auto i=difference; i<0; i++)
        std::allocator_traits<decltype(m_alloc)>::destroy(m_alloc, &m_ptr[m_size + i]);
      std::allocator_traits<decltype(m_alloc)>::deallocate(m_alloc, &m_ptr[m_size + difference],-difference);
      for(auto i=0; i<other.m_size; i++)
        m_ptr[i] = other.m_ptr[i];
    }
    else if(difference > 0)
    {
      for(auto i=0; i<m_size; i++)
        std::allocator_traits<decltype(m_alloc)>::destroy(m_alloc, &m_ptr[i]);
      std::allocator_traits<decltype(m_alloc)>::deallocate(m_alloc, m_ptr,m_size);
      m_ptr=std::allocator_traits<decltype(m_alloc)>::allocate(m_alloc, other.m_size);
      for(auto i=0; i<other.m_size; i++)
        std::allocator_traits<decltype(m_alloc)>::construct(m_alloc, &m_ptr[i],other.m_ptr[i]);
    }
    m_last=other.m_last;
    m_size=other.m_size;
    return *this;
  };

  vector& operator=(const vector&& other)
  {
    std::allocator_traits<decltype(m_alloc)>::deallocate(m_alloc, m_ptr,m_size);

    //Move ptr and set other values
    m_ptr=other.m_ptr;
    m_last=other.m_last;
    m_size=other.m_size;

    //Clean other
    other.m_ptr=nullptr;
    other.m_size=0;
    other.m_last=0;
    return *this;
  };

  //Element access
  T& at(size_t pos)
  {
      if (pos > m_size)
         throw std::out_of_range("Could not access element! Position outside bounds!");
      return m_ptr[pos];
  };

  T& operator[](size_t pos)
  {
      return m_ptr[pos];
  };

  const T& front()
  {
    return m_ptr[0];
  };

  const T& back()
  {
    return m_ptr[m_last -1];
  };

  T* data()
  {
    return m_ptr;
  };
  //Capacity
  bool empty(void)
  {
      return m_last == 0;
  };

  size_t size(void)
  {
      return m_last;
  };

  void reserve(size_t new_size)
  {
      if(new_size > m_size)
      {
          T *ptr = std::allocator_traits<decltype(m_alloc)>::allocate(m_alloc, new_size);
          for(auto i=0; i<m_size; i++){
              std::allocator_traits<decltype(m_alloc)>::construct(m_alloc, &ptr[i],m_ptr[i]);
              std::allocator_traits<decltype(m_alloc)>::destroy(m_alloc, &m_ptr[i]);
          }
          if(m_size>0)
            std::allocator_traits<decltype(m_alloc)>::deallocate(m_alloc, m_ptr, m_size);
          m_ptr=ptr;
          m_size = new_size;
      }
  };

  size_t capacity(void)
  {
      return m_size;
  };

  void shrink_to_fit()
  {

    if(m_size != m_last)
    {
      T *ptr = std::allocator_traits<decltype(m_alloc)>::allocate(m_alloc, m_last);
      for(auto i=0; i<m_size; i++)
        std::allocator_traits<decltype(m_alloc)>::construct(m_alloc, &ptr[i],m_ptr[i]);
      std::allocator_traits<decltype(m_alloc)>::deallocate(m_alloc, m_ptr, m_size);
      m_size=m_last;
      m_ptr = ptr;
    }

  }

  //Modifiers
  void push_back(const T val)
  {
      if (m_last == m_size) //Should the vector be filled, expand size
      {
          size_t size = m_size == 0 ? 1 : m_size * 2;
          T *ptr = std::allocator_traits<decltype(m_alloc)>::allocate(m_alloc, size);
          for(auto i=0; i<m_size; i++)
          {
              std::allocator_traits<decltype(m_alloc)>::construct(m_alloc, &ptr[i],m_ptr[i]);
              std::allocator_traits<decltype(m_alloc)>::destroy(m_alloc, &m_ptr[i]);
          }
          if(m_size>0)
            std::allocator_traits<decltype(m_alloc)>::deallocate(m_alloc, m_ptr, m_size);
          m_ptr=ptr;
          m_size = size;
      }

      m_ptr[m_last++] = val;
  };

  void pop_back(void)
  {
      if(m_last == 0)
          throw std::runtime_error("Empty vector!");
      std::allocator_traits<decltype(m_alloc)>::destroy(&m_ptr[m_last--]);
  };

  bool operator==(const vector<T, Allocator>& v)
  {
    if(v.m_last != this->m_last)
      return false;
    for(auto i=0; i<v.m_last;i++)
      if(v.m_ptr[i] != this->m_ptr[i])
          return false;
    return true;
  };
};
}
#endif /*STLVECTOR_H_*/
